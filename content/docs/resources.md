---
title: "Resources"

---

# Useful Resources
### Amazon Fire HD 8 (2020) Wall Mount
This is a snap to fit flush mount wall adapter for Amazon Fire HD8 2020 edition. It is recommended to print it in a standing position _(see below)_ using PETG as printing material to ensure the case is durable yet remains flexible.

{{< hint info>}}
**Material:** PETG  
**Layer Height:** 0.2mm  
**Use Support:** Yes  
**Bed Adhesion:** Raft _(recommended)_  
{{< /hint >}}

{{< tabs "uniqueid" >}}
{{< tab "Render View" >}}
![3D Rendering](/downloads/3d_print/FireHD8_2020/fire_hd8_2020_wall_mount_render.png "3D Rendering")
{{< /tab >}}

{{< tab "Print Orientation" >}}
![Print Orientation](/downloads/3d_print/FireHD8_2020/fire_hd8_2020_wall_mount_print_orientation.png "Print Orientation")
{{< /tab >}}

{{< tab "Example 1" >}}
![Example 1](/downloads/3d_print/FireHD8_2020/fire_hd8_2020_wall_mount_picture1.jpg "Example 1")
{{< /tab >}}

{{< tab "Example 2" >}}
![Example 2](/downloads/3d_print/FireHD8_2020/fire_hd8_2020_wall_mount_picture2.jpg "Example 2")
{{< /tab >}}

{{< tab "Example 3" >}}
![Example 3](/downloads/3d_print/FireHD8_2020/fire_hd8_2020_wall_mount_picture3.jpg "Example 3")
{{< /tab >}}
{{< /tabs >}}

{{< button href="/downloads/3d_print/FireHD8_2020/fire_hd8_2020_wall_mount.stl" >}}Download .stl{{< /button >}}
