---
title: "Configuration"

weight: 20
---

# Configuration

## General
### Background Access
Yakk must be allowed to run in **Unrestricted** battery optimization mode to avoid being killed. Without this the battery optimizer might kill the app either immediately after the screen goes dark or after
a given user inactivity period. Tapping this entry will ask for permission to exclude Yakk from battery optimizations.  
If permission is not granted an orange warning sign will be displayed otherwise a green checkmark. 

{{< hint danger>}}
As long as Yakk is not running in unrestricted battery optimization mode it might not function properly, please make sure Background Access is configured first thing after installation.
{{< /hint >}}

### Start after Boot
Can be turned on to start Yakk automatically after device restarts. 
{{< hint info >}}
**Default:** Off  
**Requires permission:** Display on top of other Apps
{{< /hint >}}

### Companion App
An optional companion app that will be launched after the device has booted along with Yakk. Due to newer Android version's restrictions there's a small set of whitelisted apps that are currently supported.
Reach out to **info@bkappz.com** if you want an App to be included in this list
{{< hint info >}}
**Dependency:** Start on Boot  
**Default:** Disabled  
**Choices:**
* Home Assistant
* SmartThings
* Google Home
{{< /hint >}}

## Motion Detection
### Enable Motion Detect
Enables motion detection using the device's front camera
{{< hint info>}}
**Default:** Off  
**Requires permission:** Camera
{{< /hint >}}
{{< hint warning >}}
Device **must** have a front camera present!
{{< /hint >}}

### Threshold
The required minimum relative difference between a reference and the current captured frame. This value is controlling the motion detector's general sensitivity where lower values mean higher sensitivity.

{{< hint info>}}
**Default:** 10  
**Range:** 1 - 50
{{< /hint >}}

### Latch Time
Will trigger motion detection after there is a continuous movement for at least the configured amount of time
{{< hint info>}}
**Default:** 100ms  
**Range:** 10 - 5000ms
{{< /hint >}}

## Camera Streaming
This section turns your device into a security camera, by default it is turned off
### Enable Camrea Stream
Enable MJPEG compatible camera stream listening on port 2112. You must either assign a hostname to your device or use its 
IP address. Navigate to `http://device-ip-address:2112/stream` in your browser to view the camera stream.  
Many home automation platforms have integrations that are compatible with MJPEG camera streams e.g. Home Assistant's MJPEG Camera integration

{{< hint info>}}
**Default:** Off  
**Requires permission:** Camera
{{< /hint >}}

### Show Overlay
Enable date and time overlay for the camera stream. The overlay will show the current date and time data rendered on top of the video stream using the device's default locale. The size and location of the overlay is configurable.
{{< hint info>}}
**Default:** Off
{{< /hint >}}

{{< details title="Example">}}
Overlay for US locale using default settings (_Overlay Position: Top Left and Overlay Size: Normal_)

![Default Overlay](/example_camera_overlay.png "Default overlay")
{{< /details >}}

### Overlay Position
Controls the position on the screen where the overlay text will be rendered, there are four predefined positions to choose from `Top Left`, `Bottom Left`, `Top Right` and `Bottom Right`
{{< hint info>}}
**Default:** Top Left
{{< /hint >}}

### Overlay Size
Controls the size of the overlay text, currently there are three predefined sizes to choose from `Small`, `Normal` and `Large`
{{< hint info>}}
**Default:** Normal
{{< /hint >}}

## Advanced Settings
This section is under Camera Preview screen.
### Luma Sensitivity
Pixel brightness threshold used for motion detection. The default value should work most of the time, although it might need to be changed if the device is installed in an unusually low-contrast environment. E.g. people wearing clothing that is similar
to the background's color or there's a bright light source in the background like a large window.  

The best way to adjust this is by first turning on *Show Motion Mask* and actively checking the motion mask's area while having moving objects in the frame.
{{< hint warning >}}
Setting the value too low will cause the motion mask to pick up camera noise while setting it too high will cause no motion mask to be generated at all.
{{< /hint>}}

{{< hint info>}}
**Default:** 30  
**Range:** 1-255
{{< /hint >}}

#### Example
{{< tabs "uniqueid" >}}
{{< tab "Too Low Luma" >}}
Too much camera noise is being picked up resulting in an erratic motion mask even when there are no moving objects on the image 

![Too Low Luma](/example_too_low_luma.png "Too much camera noise")
{{< /tab >}}

{{< tab "Too High Luma" >}}
Motion mask is almost non-existent, despite the object on the image moving fast only a very tiny motion mask is generated at
one of the fingertips.

![Too High Luma](/example_too_high_luma.png "Almost non existent motion mask")
{{< /tab >}}

{{< tab "Correct Luma" >}} 
Perfect motion mask clustering around the boundaries of the moving object. The motion mask's shape should resemble to the moving 
object's shape.

![Correct Luma](/example_correct_luma.png "Adequate motion mask")
{{< /tab >}}
{{< /tabs >}}
