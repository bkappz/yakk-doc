---
title: "FAQ"

weight: 1
---
# Frequently Asked Questions

## Motion is detected but screen does not unlock
Lock screen type must be set to `None` and there should be no `Wallpaper` configured either. Yakk can not unlock the screen if it is secured with either a Pin, Pattern, Biometric or **Swipe** unlock  
Navigate to **Settings &rarr; Lock Screen &rarr; Screen Lock Type** and set type to **None** 

## Screen randomly stops unlocking
Yakk holds a request on keyguard dismissal the entire time it is running. Due to undermentioned power optimizations the system might take this grant away unexpectedly. To avoid this please make sure *Background* Access is granted.  
If motion detection is stuck you can disable Motion Detect and MJPEG Services then re-enable them which will force Yakk to obtain the grant again.

## Why is motion detection not working after a while?
Yakk must be allowed to run in **Unrestricted** battery optimization mode to avoid being killed. You can grant this by tapping on
*Background Access* and granting Yakk to be excluded from battery optimizations. There is an orange warning sign displayed if permission is not granted or a green checkmark otherwise. If Yakk is still being killed try to turn off OEM specific, non standard battery optimization features as well.

e.g. on Samsung devices if still having issues turn off **Put unused apps to sleep** under **Settings &rarr; Battery and device care &rarr; Background usage limits**  

## Why there's no motion detected?
Even though Yakk comes with sane default configurations it is sometimes necessary to adjust the built in thresholds. You can either change the configuration on the main screen or navigate to the `Camera Preview` screen and adjust the sliders there. All changes made on the sliders are permanent and will be used by the motion detector.  
The `Camera Preview` screen provides a more interactive way to configure Yakk. Enabling the `Motion Mask` checkbox will also display the detected motion mask. If you see no motion mask you might need to adjust *Luma Sensitivity* until you're satisfied.


