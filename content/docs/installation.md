---
title: "Installation"

weight: 10
---

# Installation
{{< hint info >}}
Yakk can be installed and used offline, however built-in automatic error reporting will not work. If you encounter issues in offline environments please reach out on info@bkappz.com email address
{{< /hint >}}

## Google Play
The most convenient and easiest way to install Yakk is through Google Play. Application updates are also managed by the Google Play services so you will never miss an important update.  
{{< rawhtml >}}
<div style="display: grid; justify-content: center">
    <a href='https://play.google.com/store/search?q=yakk&c=apps&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png' width="225px"/></a>
</div>
{{< /rawhtml >}}

## Direct Download
The latest release can also be downloaded below. This APK file is the same universally signed APK that is installed through Google Play. Therefore, you can switch to the Google Play installation method anytime without uninstalling the app first.

{{< rawhtml >}}
<script type="application/javascript">
  fetch('https://static.bkappz.com/yakk/version.json')
    .then((response) => response.json())
    .then((json) => {
        const btn = document.getElementById("download-yakk-btn");
        btn.href = json.updateUrl
        btn.innerText = "Download Yakk (v" + json.version_name + ")"
        btn.style.display = "inline-block"
    });
</script>
<div style="display: grid; justify-content: center">
    <a id="download-yakk-btn" target="_blank" rel="noopener" class="book-btn" style="display: none; line-height: 3.5rem; padding: 0 1.2rem">Download Yakk</a>
</div>
{{< /rawhtml >}}

{{< hint warning >}}
Automatic app updates will not work with Direct Download installation method
{{< /hint >}}
