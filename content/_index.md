---
title: "Yakk Documentation"

description: ""
---

# Yakk

Simple and lightweight companion app for wall mounted or stationary devices used in home automation. Turn your device into a security camera and a motion detection sensor at the same time. 

Yakk has a very small footprint and extremely ligthweight making it perfectly suitable to run on not just the latest and greatest but older, low end devices in environments where full-blown KIOSK functionalities are not required.

Yakk runs in the background, parallel with your primary content provider app *(e.g. Home Assistant Companion app)* waking up the device when motion is detected.  
It does not try to embed or render your preferred content leaving that to other apps that are better suited and purpose built for that task.

![Yakk Logo](/yakk-logo.png#yakk-logo)

## Features
___

{{< columns >}}
### Motion Detection
Yakk will let your device turning off its screen and allowing it to enter low power mode, but still wake it up when movement is detected

<--->

### Auto Start
Automatically start Yakk and an optional companion app after device boot

<--->
### Camera Streaming 
Turn your device into a security camera with Yakk by streaming an MJPEG video feed. Supports up to **10** concurrent sessions at the same time
{{< /columns >}}
___

See [Installation]({{< ref "/docs/installation" >}}) for installation instructions or [Configuration]({{< ref "/docs/configuration" >}}) page for the full list of configuration options.

Yakk stands for "**Y**et **A**nother **K**iosk **C**ompanion" due to lack of imagination to come up with a better name

Yakk is completely free of charge and ad-free, if you'd still like to support the project you can buy me a coffee. Thank you! 
{{< rawhtml >}}
<script type="text/javascript" src="https://cdnjs.buymeacoffee.com/1.0.0/button.prod.min.js" data-name="bmc-button" data-slug="bkappz" data-color="#FFDD00" data-emoji=""  data-font="Cookie" data-text="Buy me a coffee" data-outline-color="#000000" data-font-color="#000000" data-coffee-color="#ffffff" ></script>
{{< /rawhtml >}}
